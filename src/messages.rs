use waef_core::core::{IsMessage, Message};
use waef_core::util::pod::Pod;

pub struct MountSoundBuffer {
    pub sound_buffer_id: u128,
    pub data: Vec<u8>,
}
impl IsMessage for MountSoundBuffer {
    fn into_message(self) -> Message {
        let mut data = Vec::with_capacity(self.data.len() + 16);
        data.extend(self.sound_buffer_id.try_split());
        data.extend(self.data);
        Message::new_from_boxed_bytes(Self::category_name(), data.into_boxed_slice())
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Some(Self {
            sound_buffer_id: u128::from_bytes(&msg.buffer[0..16]),
            data: msg.buffer[16..].to_vec(),
        })
    }

    fn category_name() -> &'static str {
        "kira:buffer:mount"
    }
}

pub struct UnmountSoundBuffer {
    pub sound_buffer_id: u128,
}
impl IsMessage for UnmountSoundBuffer {
    fn into_message(self) -> Message {
        Message::new_from_pod(Self::category_name(), self.sound_buffer_id)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Some(Self {
            sound_buffer_id: u128::from_bytes(&msg.buffer[0..16]),
        })
    }

    fn category_name() -> &'static str {
        "kira:buffer:unmount"
    }
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct PlaySound {
    pub sound_buffer_id: u128,
    pub sound_id: u128,
    pub volume: f64,
    pub loops: bool,
}
unsafe impl Pod for PlaySound {}

impl IsMessage for PlaySound {
    fn into_message(self) -> Message {
        Message::new_from_pod(Self::category_name(), self)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Some(Self::from_bytes(&msg.buffer))
    }

    fn category_name() -> &'static str {
        "kira:sound:play"
    }
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct StopSound {
    pub sound_id: u128,
}
unsafe impl Pod for StopSound {}

impl IsMessage for StopSound {
    fn into_message(self) -> Message {
        Message::new_from_pod(Self::category_name(), self)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Some(Self::from_bytes(&msg.buffer))
    }

    fn category_name() -> &'static str {
        "kira:sound:stop"
    }
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct OnSoundStopped {
    pub sound_id: u128,
}
unsafe impl Pod for OnSoundStopped {}

impl IsMessage for OnSoundStopped {
    fn into_message(self) -> Message {
        Message::new_from_pod(Self::category_name(), self)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Some(Self::from_bytes(&msg.buffer))
    }

    fn category_name() -> &'static str {
        "kira:sound:on_stopped"
    }
}
