use std::{collections::HashMap, io::Cursor, sync::mpsc::Sender};

use crate::messages::{MountSoundBuffer, OnSoundStopped, PlaySound, StopSound, UnmountSoundBuffer};
use kira::{
    manager::{backend::DefaultBackend, AudioManager, AudioManagerSettings},
    sound::{
        static_sound::{StaticSoundData, StaticSoundHandle},
        PlaybackState, Region,
    },
    tween::{Tween, Value},
    Volume,
};
use serde::{Deserialize, Serialize};
use symphonia::core::io::ReadOnlySource;
use waef_core::{
    actors::{ActorAlignment, ActorsPlugin, HasActorAlignment, IsActor, UseActors},
    core::{ExecutionResult, IsDispatcher, IsDisposable, IsMessage, Message, WaefError},
    DispatchFilter,
};

pub struct KiraActor {
    dispatcher: Sender<Message>,
    manager: AudioManager,
    loaded_sounds: HashMap<u128, StaticSoundData>,
    playing_sounds: HashMap<u128, StaticSoundHandle>,
}

impl KiraActor {
    fn new(dispatcher: Sender<Message>) -> Result<Self, WaefError> {
        Ok(Self {
            dispatcher,
            manager: AudioManager::<DefaultBackend>::new(AudioManagerSettings::default())
                .map_err(WaefError::new)?,
            loaded_sounds: HashMap::new(),
            playing_sounds: HashMap::new(),
        })
    }

    fn mount_sound_buffer(&mut self, data: MountSoundBuffer) -> ExecutionResult {
        let _guard = tracing::trace_span!(
            "kira:mount_sound_buffer",
            sound_buffer_id = data.sound_buffer_id,
            sound_buffer_size = data.data.len()
        )
        .entered();

        let buffer = Cursor::new(data.data);
        let media_source = ReadOnlySource::new(buffer);
        match StaticSoundData::from_media_source(media_source) {
            Ok(sound) => {
                self.loaded_sounds.insert(data.sound_buffer_id, sound);
                ExecutionResult::Processed
            }
            Err(err) => {
                tracing::error!(
                    "Failed to load sound buffer for id {}: {:?}",
                    data.sound_buffer_id,
                    err
                );
                ExecutionResult::Processed
            }
        }
    }

    fn unmount_sound_buffer(&mut self, sound_buffer_id: u128) -> ExecutionResult {
        let _guard = tracing::trace_span!("kira:unmount_sound_buffer", sound_buffer_id).entered();

        self.loaded_sounds.remove(&sound_buffer_id);
        ExecutionResult::Processed
    }

    fn play_sound(&mut self, request: PlaySound) -> ExecutionResult {
        let _guard = tracing::trace_span!(
            "kira:play_sound_buffer",
            sound_buffer_id = request.sound_buffer_id,
            sound_id = request.sound_id
        )
        .entered();

        if let Some(sound_data) = self.loaded_sounds.get(&request.sound_buffer_id) {
            let mut sound_data = sound_data.clone();
            if request.loops {
                sound_data.settings.loop_region = Some(Region::default());
            }
            sound_data.settings.volume = Value::Fixed(Volume::Amplitude(request.volume));

            match self.manager.play(sound_data) {
                Ok(playing) => {
                    if let Some(mut previous) =
                        self.playing_sounds.insert(request.sound_id, playing)
                    {
                        previous.stop(Tween::default());
                    }
                }
                Err(err) => {
                    tracing::error!(
                        "Failed to play sound buffer for id {}: {:?}",
                        request.sound_buffer_id,
                        err
                    );
                }
            }
        }
        ExecutionResult::Processed
    }

    fn stop_sound(&mut self, sound_id: u128) -> ExecutionResult {
        if let Some(sound) = self.playing_sounds.get_mut(&sound_id) {
            sound.stop(Tween::default());
        }
        ExecutionResult::Processed
    }

    pub fn tick(&mut self) -> ExecutionResult {
        let _guard = tracing::trace_span!("kira:tick_sounds").entered();

        let mut finished_sounds = Vec::new();
        for (sound_id, sound_info) in self.playing_sounds.iter() {
            if sound_info.state() == PlaybackState::Stopped {
                finished_sounds.push(*sound_id);
            }
        }
        for sound_id in finished_sounds {
            self.playing_sounds.remove(&sound_id);
            _ = self
                .dispatcher
                .send(OnSoundStopped { sound_id }.into_message());
        }
        ExecutionResult::Processed
    }
}

impl IsActor for KiraActor {
    fn weight(&self) -> u32 {
        4
    }

    fn name(&self) -> String {
        "kira".to_string()
    }
}
impl std::fmt::Debug for KiraActor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("KiraActor").finish()
    }
}

#[cfg(target_arch = "wasm32")]
unsafe impl Send for KiraActor {}

#[cfg(target_arch = "wasm32")]
unsafe impl Sync for KiraActor {}

impl IsDispatcher for KiraActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        match message.name.as_str() {
            "kira:buffer:mount" => MountSoundBuffer::from_message(message)
                .map(|d| self.mount_sound_buffer(d))
                .unwrap_or(ExecutionResult::NoOp),

            "kira:buffer:unmount" => UnmountSoundBuffer::from_message(message)
                .map(|d| self.unmount_sound_buffer(d.sound_buffer_id))
                .unwrap_or(ExecutionResult::NoOp),

            "kira:sound:play" => PlaySound::from_message(message)
                .map(|s| self.play_sound(s))
                .unwrap_or(ExecutionResult::NoOp),

            "kira:sound:stop" => StopSound::from_message(message)
                .map(|s| self.stop_sound(s.sound_id))
                .unwrap_or(ExecutionResult::NoOp),

            "app:tick" => self.tick(),

            _ => ExecutionResult::NoOp,
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf(["kira".into(), "app:tick".into()].into())
    }
}

impl IsDisposable for KiraActor {
    fn dispose(&mut self) {}
}

impl HasActorAlignment for KiraActor {
    fn alignment(&self) -> ActorAlignment {
        ActorAlignment::Priority
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct KiraCfg {
    pub global_volume: f32,
}
impl Default for KiraCfg {
    fn default() -> Self {
        Self { global_volume: 1.0 }
    }
}

pub struct KiraPlugin {
    pub cfg: KiraCfg,
    pub dispatcher: Sender<Message>,
}
impl KiraPlugin {
    pub fn new(dispatcher: Sender<Message>, cfg: KiraCfg) -> Self {
        Self { cfg, dispatcher }
    }
}
impl ActorsPlugin for KiraPlugin {
    fn apply<T: UseActors>(self, target: T) -> T {
        match KiraActor::new(self.dispatcher) {
            Ok(actor) => target.use_actor(Box::new(actor)),
            Err(err) => {
                tracing::error!("Failed to create Kira actor: {:?}", err);
                target
            }
        }
    }
}
